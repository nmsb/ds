/*
 * An ordered list of integers, without repetion.
 *
 * Copyright (C) 2024 Nuno Baeta
 * Distributed under GNU GPL 3.0 or later.
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

struct ordered_list {
	int i;
	struct ordered_list *next;
};

void create(struct ordered_list **);
bool is_empty(struct ordered_list *);
void insert(struct ordered_list **, int);
void display(struct ordered_list *);

int main(void)
{
	int option, i;
	struct ordered_list *ol;

	printf("An ordered list of integers without repetition\n");
	printf("==============================================\n");
	create(&ol);
	do {
		printf("\n");
		printf("Options:\n");
		printf("    1. Insert     2. Remove     3. Empty\n");
		printf("    4. Display    0. End\n");
		printf("Choose option: ");
		scanf("%d", &option);
		printf("\n");
		switch (option) {
		case 1:
			printf("Element: ");
			scanf("%d", &i);
			insert(&ol, i);
			printf("Element %d inserted in the ordered list.\n", i);
			break;
		case 3:
			if (empty(ol))
				printf("The list is empty.\n");
			else
				printf("The list is not empty.\n");
			break;
		case 4:
			if (empty(ol))
				printf("The list is empty.\n");
			else
				display(ol);
			break;
		case 0:
			break;
		default:
			printf("Option not available.\n");
		}
	} while (option != 0);
	
	return EXIT_SUCCESS;
}

/*
 * Creates an empty list.
 */
void create(struct ordered_list **ol)
{
	*ol = NULL;
}

/*
 * Checks if the list is empty.
 */
bool is_empty(struct ordered_list *ol)
{
	return ol == NULL;
}

/*
 * Inserts an integer in the correct position of the ordered list.
 */
void insert(struct ordered_list **ol, int i)
{
	struct ordered_list *new_i, *curr, *prev;

	new_i = (struct ordered_list *) malloc(sizeof(struct ordered_list));
	new_i->i = i;
	
	/* if (*ol == NULL) */
	/* 	aux->next = NULL; */
	/* else */
	/* 	aux->next = *ol; */
	/* *ol = aux; */
}

/*
 * Displays the list.
 */
void display(struct ordered_list *ol)
{
	printf("The list is:\n");
	printf("\n");
	while (ol != NULL) {
		printf("%d\t", ol->i);
		ol = ol->next;
	}
	printf("\n");
}
