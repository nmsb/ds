/*
 * A stack of integers.
 *
 * Copyright (C) 2024 Nuno Baeta
 * Distributed under GNU GPL 3.0 or later.
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

struct stack_node {
	int i;
	struct stack_node *next;
};

void create(struct stack_node **);
void push(struct stack_node **, int);
void pop(struct stack_node **);
int top(struct stack_node *);
bool is_empty(struct stack_node *);
int size(struct stack_node *);
void show(struct stack_node *);

int main(void)
{
	int option, i;
	struct stack_node *s;

	printf("A stack of integers\n");
	printf("===================\n");
	create(&s);
	do {
		printf("\n");
		show(s);
		printf("\n");
		printf("(1) Push | (2) Pop | (3) Top | (0) End : ");
		scanf("%d", &option);
		switch (option) {
		case 1:
			printf("\n");
			printf("Element: ");
			scanf("%d", &i);
			push(&s, i);
			break;
		case 2:
			if (is_empty(s)) {
				printf("\n");
				printf("Impossible, the stack is empty.\n");
			} else {
				pop(&s);
			}
			break;
		case 3:
			if (is_empty(s)) {
				printf("\n");
				printf("Impossible, the stack is empty.\n");
			} else {
				printf("\n");
				printf("Top of the stack: %d\n", top(s));
			}
			break;
		case 0:
			break;
		default:
			printf("\n");
			printf("Option not available.\n");
		}
	} while (option != 0);

	return EXIT_SUCCESS;
}

/*
 * Creates an empty stack.
 */
void create(struct stack_node **s)
{
	*s = NULL;
}

/*
 * Pushes an integer to the stack.
 */
void push(struct stack_node **s, int i)
{
	struct stack_node *p;

	p = (struct stack_node *) malloc(sizeof(struct stack_node));
	p->i = i;
	p->next = *s;
	*s = p;
}

/*
 * Removes the integer on the top of the stack.  Does not check if the
 * stack is empty.
 */
void pop(struct stack_node **s)
{
	struct stack_node *p;

	p = *s;
	*s = (*s)->next;
	free(p);
}

/*
 * Obtains the integer on the top of the stack.  Does not check if the
 * stack is empty.
 */
int top(struct stack_node *s)
{
	return s->i;
}

/*
 * Checks if the stack is empty.
 */
bool is_empty(struct stack_node *s)
{
	return s == NULL;
}

/*
 * Number of integers in the stack.
 */
int size(struct stack_node *s)
{
	int i = 0;
	while (s != NULL) {
		i++;
		s = s-> next;
	}
	return i;
}

/*
 * Shows the stack's integers.
 */
void show(struct stack_node *s)
{
	struct stack_node *p;

	p = s;
	if (p == NULL) {
		printf("The stack is empty.  ");
	} else {
		printf("Stack:  ");
		while (p != NULL) {
			printf("%d  ", p->i);
			p = p->next;
		}
	}
	printf("(%d elements)\n", size(s));
}
