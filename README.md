# Data Stuctures

Experiments with data structures in different languages.

---

- The name of each directory reflects the language used.
- The *useful* data (of the data structures) is always integers.
- The code provided in this repository is distributed under [GNU GPL 3.0](https://www.gnu.org/licenses/gpl-3.0.html.en) or later.
